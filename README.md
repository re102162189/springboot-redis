##### Create Redis
```
docker run --name redis \
	-p 6379:6379 \
	-d redis:6.2.6
```

##### GET/POST
```
curl http://localhost:8080/redis/key/test

curl -d '{"salary": 2600, "name": "kite"}' \
	-H "Content-Type: application/json" \
	-X POST http://localhost:8080/redis/key/test
```