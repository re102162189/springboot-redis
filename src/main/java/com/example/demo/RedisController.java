package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redis")
public class RedisController {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@GetMapping("/key/{key}")
	public Object getValue(@PathVariable String key) {
		ValueOperations<String, Object> getOps = redisTemplate.opsForValue();
		return getOps.get(key);
	}

	@PostMapping("/key/{key}")
	public Object setValue(@PathVariable String key, @RequestBody Object value) {
		ValueOperations<String, Object> getOps = redisTemplate.opsForValue();
		getOps.set(key, value);
		return value;
	}
}
